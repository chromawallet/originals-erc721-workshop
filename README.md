# Workshop with External ERC721 on Originals

## Requirements

* Docker (if Linux, then docker-compose as well)
* Node

## Modifying Avatars
You can modify the avatars that gets minted by updating `avatars.json`.

## Start

```sh
# Install script dependencies
npm i

# Start (& restarts) the blockchain and originals metadata resolver
npm start

# Mint tokens
npm run mint
```

## Stop

```sh
# Stops the blockchain and originals metadata resolver
npm stop
```