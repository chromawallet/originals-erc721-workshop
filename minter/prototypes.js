const { Text, createPrototype, IExternal } = require('./lib');
const { IAvatar } = require('./interfaces');

require('dotenv').config();

const description = 'Avatars to the Public!';

const Avatar = 'Avatar';

async function createPlotPrototype(blockchain, user, auth) {
  await createPrototype(
    blockchain,
    user,
    auth,
    IAvatar,
    [
      [[IExternal, 'chain'], Text('bnb')],
      [[IExternal, 'contract_address'], Text(process.env.ERC721_CONTRACT)],
      [[IExternal, 'contract_type'], Text('ERC721')],
      [[IExternal, 'description'], Text(description)],
      [[IExternal, 'external_url'], Text('https://www.chromia.com')],
    ],
    Avatar
  );
}

module.exports = {
  createPlotPrototype,
  Avatar,
};
