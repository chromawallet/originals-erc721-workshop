const config = require("./config");
const http = require("http");
const https = require("https");
const { prompt } = require("inquirer");
const { Postchain } = require("ft3-lib");

async function getBlockchainConfig() {
  const environments = Object.keys(config.environments);

  const { environment } = await prompt({
    type: "list",
    name: "environment",
    message: "Select environment",
    choices: environments,
  });

  const node = config.environments[environment].node;
  const brid = node.brid || (await getBlockchainBrid(node.url, node.iid));

  return { url: node.url, brid };
}

async function getBlockchain() {
  const { url, brid } = await getBlockchainConfig();

  return await new Postchain(url).blockchain(brid);
}

async function getBlockchainBrid(url, iid) {
  return new Promise((resolve, reject) => {
    if (url === undefined) return reject(new Error("Missing node url"));
    if (iid === undefined) return reject(new Error("Missing node iid"));
    (url.startsWith("https") ? https : http)
      .get(`${url}/brid/iid_${iid}`, (response) => {
        response.on("data", (brid) => {
          resolve(brid.toString());
        });
      })
      .on("error", (error) => {
        reject(error);
      });
  });
}

module.exports = {
  getBlockchain,
  getBlockchainBrid,
};
