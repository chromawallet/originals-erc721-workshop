const { op } = require("ft3-lib");
const { gtv } = require("postchain-client");

const IExternal = "chromia.IExternal";

async function createInterface(
  blockchain,
  user,
  auth,
  interface,
  attributes,
  dependencies = [],
  meta = [],
  isOpen = false
) {
  const processedAttributes = Object.keys(attributes).map((key) => [
    [interface, key],
    attributes[key],
  ]);

  await blockchain.call(
    op(
      "def_interface_op",
      auth,
      interface,
      processedAttributes,
      dependencies,
      meta,
      isOpen
    ),
    user
  );
}

async function createPrototype(
  blockchain,
  user,
  auth,
  interface,
  attributes,
  prototypeName,
  prototypeParent = null
) {
  await blockchain.call(
    op(
      "def_prototype_op",
      auth,
      interface,
      attributes,
      prototypeName,
      prototypeParent
    ),
    user
  );
}

const a_type = {
  boolean: 0,
  byte_array: 1,
  decimal: 2,
  id: 3,
  instance: 4,
  integer: 5,
  text: 6,
  unordered_container: 7, // set
  ordered_container: 8, // list
  addressable_container: 9,
  file: 10,
  instance_def: 11,
};

function Type(type, meta = []) {
  return [type, meta];
}

function Text(value = null) {
  return [a_type.text, value, null];
}

function ByteArray(value) {
  return [a_type.byte_array, value, null];
}

function Integer(value = null) {
  return [a_type.integer, value, null];
}

function Decimal(value = null) {
  return [a_type.decimal, value, null];
}

function ListInstanceDef(instanceDefs) {
  return [a_type.ordered_container, instanceDefs, a_type.instance_def];
}

function ListInteger(elements = null) {
  return [a_type.ordered_container, elements, a_type.integer];
}

function ListInstance(elements = null) {
  return [a_type.ordered_container, elements, a_type.instance];
}

function InstanceDef(interface, attributes, prototype = null) {
  return [a_type.instance_def, [interface, attributes, prototype], null];
}

function File(hash, brid, location) {
  return [a_type.file, [hash, brid, location], null];
}

function MetadataAttribute() {
  return ["metadata_attribute", "", "metadata_storage"];
}

function Optional() {
  return ["optional", gtv.encodeGtv(1), ""];
}

module.exports = {
  createPrototype,
  createInterface,
  Type,
  Text,
  ByteArray,
  Integer,
  Decimal,
  ListInstanceDef,
  ListInstance,
  ListInteger,
  InstanceDef,
  File,
  MetadataAttribute,
  Optional,
  IExternal,
};
