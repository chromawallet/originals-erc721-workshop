const { readdir, stat, writeFile } = require("fs/promises");
const { builtinModules } = require("module");
const { join, extname } = require("path");

const Types = {
  Images: [".jpeg", ".jpg", ".png", ".gif", ".tiff", ".tif"],
};

async function findFiles(options, directory, searchSubdirectories = true) {
  const files = await readdir(directory);
  const filesInfo = await Promise.all(
    files.map(async (file) => {
      const filePath = join(directory, file);
      const stats = await stat(filePath);

      return {
        name: file,
        extension: extname(file),
        path: filePath,
        size: stats.size,
        isDirectory: stats.isDirectory(),
      };
    })
  );

  const foundFiles = [];
  const directories = [];
  const types = options.types;
  for (const file of filesInfo) {
    if (file.isDirectory && searchSubdirectories) {
      directories.push(file);
    } else if (types == null || types.includes(file.extension)) {
      foundFiles.push(file);
    }
  }

  const filesInDirecotories = await Promise.all(
    directories.map((directory) =>
      findFiles(options, directory.path, searchSubdirectories)
    )
  );

  const result = foundFiles.concat(filesInDirecotories.flat());

  return result;
}

function getFoundFilesDetails(files) {
  if (!files) {
    return {
      count: 0,
      size: 0,
    };
  }

  const count = files.length;

  let totalSize = 0;
  for (const file of files) {
    totalSize += file.size;
  }

  return {
    count,
    size: totalSize,
  };
}

function getHumanReadableSize(size) {
  const units = ["KB", "MB", "GB", "TB"];

  if (size < 1024) {
    return {
      unit: "B",
      size,
    };
  }

  let s = size / 1024;
  let i = 0;

  while (s > 1024 && i < units.length) {
    i++;
    s = s / 1024;
  }

  return {
    unit: units[i],
    size: s.toFixed(2),
  };
}

async function writeJsonToFile(filename, object) {
  await writeFile(filename, JSON.stringify(object, null, 2));
}

module.exports = {
  Types,
  findFiles,
  writeJsonToFile,
  getFoundFilesDetails,
  getHumanReadableSize,
};
