const { Filehub, FsFile } = require('@snieking/fs-client');
const { readFileSync } = require('fs');
const {
  SingleSignatureAuthDescriptor,
  FlagsType,
  KeyPair,
  Postchain,
  User,
} = require('ft3-lib');

async function uploadData(data, config) {
  const user = composeUser(config.adminPrivKey);
  await registerFileHubUserIfNeeded(config, user);
  const filehub = new Filehub(config.fileHub.url, config.fileHub.brid);

  const file = FsFile.fromData(data);

  const exists = await filehub.hasFile(file.hash);
  if (!exists) {
    try {
      await filehub.storeFile(user, file);
    } catch (error) {}
  }

  return file.hash;
}

async function registerFileHubUserIfNeeded(config, user) {
  const blockchain = await new Postchain(config.fileHub.url).blockchain(
    config.fileHub.brid
  );

  const account = await blockchain.query('ft3.get_account_by_id', {
    id: user.authDescriptor.id,
  });

  if (account != null) return;

  await blockchain.registerAccount(user.authDescriptor, user);
}

function composeUser(privKey) {
  const keyPair = privKey ? new KeyPair(privKey) : new KeyPair();
  return new User(
    keyPair,
    new SingleSignatureAuthDescriptor(keyPair.pubKey, [
      FlagsType.Account,
      FlagsType.Transfer,
    ])
  );
}

module.exports = {
  uploadData,
};
