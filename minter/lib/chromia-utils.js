const {
  KeyPair,
  User,
  SingleSignatureAuthDescriptor,
  FlagsType,
} = require("ft3-lib");

function composeUser(
  privateKey,
  flags = [FlagsType.Account, FlagsType.Transfer]
) {
  const keyPair = new KeyPair(privateKey);
  return new User(
    keyPair,
    new SingleSignatureAuthDescriptor(keyPair.pubKey, flags)
  );
}

async function registerUser(blockchain, user) {
  await blockchain.registerAccount(user.authDescriptor, user);
}

module.exports = {
  composeUser,
  registerUser,
};
