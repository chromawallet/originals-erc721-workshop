const fetch = require('cross-fetch');

const avatars = require('../avatars.json');
const { IAvatar } = require('./interfaces');
const { File, IExternal, Integer, Text } = require('./lib');
const { uploadData } = require('./lib/filehub-utils');
const { Avatar } = require('./prototypes');
const { op } = require('ft3-lib');

require('dotenv').config();

async function createInstances(blockchain, user, userAuth) {
  const tx = blockchain.transactionBuilder();
  for (let i = 0; i < avatars.length; i++) {
    const response = await fetch(avatars[i].image);

    const image = await response.buffer();
    const imageType = await response.headers.get('Content-Type');

    const fileHubCfg = {
      adminPrivKey: process.env.ADMIN_PRIV_KEY,
      fileHub: {
        url: process.env.FILEHUB_URL,
        brid: process.env.FILEHUB_BRID,
      },
    };

    const file = await uploadData(image, fileHubCfg);

    tx.add(
      op(
        'def_instance_op',
        userAuth,
        IAvatar,
        [
          [[IExternal, 'external_id'], Integer(i)],
          [[IExternal, 'name'], Text(`Avatar #${i}`)],
          [
            [IExternal, 'image'],
            File(file, fileHubCfg.fileHub.brid, fileHubCfg.fileHub.url),
          ],
          [[IExternal, 'image_type'], Text(imageType)],
          [[IAvatar, 'name'], Text(avatars[i].name)],
          [[IAvatar, 'hobby'], Text(avatars[i].hobby)],
        ],
        Avatar
      ),
      user
    );
  }

  await tx.buildAndSign(user).post();
  console.log('\n');

  for (let i = 0; i < avatars.length; i++) {
    console.log(
      `${avatars[i].name} tokenUri => http://localhost:3000/bnb/${process.env.ERC721_CONTRACT}/${i}`
    );
  }
}

module.exports = {
  createInstances,
};
