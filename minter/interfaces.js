const { gtv } = require('postchain-client');

const { createInterface, Type, Text, IExternal } = require('./lib');

const IAvatar = 'IAvatar';

async function createInterfaces(blockchain, user, auth) {
  await createInterface(
    blockchain,
    user,
    auth,
    IAvatar,
    {
      name: Type(Text(), [MetadataAttributeTraitType('Name')]),
      hobby: Type(Text(), [MetadataAttributeTraitType('Hobby')]),
    },
    [IExternal]
  );
}

function MetadataAttributeTraitType(value) {
  return ['trait_type', gtv.encodeGtv(value), 'metadata_attribute'];
}

module.exports = {
  IAvatar,
  createInterfaces,
};
