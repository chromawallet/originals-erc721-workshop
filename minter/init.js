const { createInstances } = require('./minter');
const { createInterfaces } = require('./interfaces');
const { createPlotPrototype } = require('./prototypes');
const { getBlockchain } = require('./lib/chain-selector');
const { composeUser } = require('./lib/chromia-utils');

const { op } = require('ft3-lib');

require('dotenv').config();

async function main() {
  const blockchain = await getBlockchain();

  const user = composeUser(process.env.ADMIN_PRIV_KEY);
  const userAuth = [user.authDescriptor.id, user.authDescriptor.id];

  // Only needed for the first time, when running locally
  await blockchain.call(op('metadata.init', process.env.METADATA_URL), user);
  await blockchain.call(
    op('originals.init_originals_interfaces_op', userAuth),
    user
  );
  await blockchain.call(
    op('access.whitelist.whitelist_account', user.authDescriptor.id),
    user
  );
  // End of only needed for the first time

  await createInterfaces(blockchain, user, userAuth);
  await createPlotPrototype(blockchain, user, userAuth);
  await createInstances(blockchain, user, userAuth);
}

main()
  .then(() => process.exit(0))
  .catch((error) => {
    console.log(error);
    process.exit(1);
  });
