import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { BlockchainDirectoryModule } from './blockchain-directory/blockchain-directory.module';
import { RedirectDirectoryModule } from './redirect-directory/redirect-directory.module';
import { ConfigModule } from '@nestjs/config';
import dev from './config/configuration.dev';
import prod from './config/configuration.prod';

@Module({
  imports: [
    ConfigModule.forRoot({
      load: [process.env.NODE_ENV === 'production' ? prod : dev],
      isGlobal: true,
    }),
    BlockchainDirectoryModule,
    RedirectDirectoryModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule { }
