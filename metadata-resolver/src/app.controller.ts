import { Controller, Get, Logger, Param, Response } from '@nestjs/common';
import { ApiTags } from '@nestjs/swagger';
import { AppService } from './app.service';
import { Response as Res } from 'express';

@ApiTags('resolver')
@Controller()
export class AppController {
  private readonly logger = new Logger(AppController.name);

  constructor(private readonly appService: AppService) { }

  @Get(':chain/:contract/:id')
  async getAttributes(
    @Param('chain') chain: string,
    @Param('contract') contract: string,
    @Param('id') id: string,
    @Response() res: Res,
  ): Promise<Res> {
    const redirectUrl = this.appService.getRedirectUrl(chain, contract, id);
    if (redirectUrl != null) {
      res.redirect(302, redirectUrl);
      return;
    } else {
      const attributes = await this.appService.getAttributes(chain, contract, id);
      this.logger.debug(
        `${chain}/${contract}/${id} resolved attributes: ${JSON.stringify(
          attributes,
        )}`,
      );
      return res.send(attributes);
    }

  }

  @Get(':chain/:contract/:id/image')
  async getImage(
    @Param('chain') chain: string,
    @Param('contract') contract: string,
    @Param('id') id: string,
    @Response() res: Res,
  ): Promise<Res> {
    const redirectUrl = this.appService.getRedirectUrl(chain, contract, id);
    if (redirectUrl != null) {
      res.redirect(302, redirectUrl + "/image");
      return;
    } else {
      const image = await this.appService.getImage(chain, contract, id);

      this.logger.debug(
        `${chain}/${contract}/${id}/image size: ${image.data.length}}`,
      );

      return res.set({ 'Content-Type': image.contentType }).send(image.data);
    }
  }

  @Get('health')
  health(): string {
    return 'OK';
  }
}
