import { Injectable } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { ContractRedirectMapping } from 'src/models/contract-mapping.model';

@Injectable()
export class RedirectDirectoryService {
  private redirects: ContractRedirectMapping[];

  constructor(private readonly config: ConfigService) {
    const mappings = this.config.get<ContractRedirectMapping[]>('contractRedirectMappings');
    this.redirects = mappings;
  }

  getUrl(contract: string): string {
    const redirect = this.redirects.find(r => r.contract == contract.toLowerCase());

    return redirect == undefined ? null : redirect.url;
  }
}
