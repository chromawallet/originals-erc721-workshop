import { Module } from '@nestjs/common';
import { RedirectDirectoryService } from './redirect-directory.service';

@Module({
  providers: [RedirectDirectoryService],
  exports: [RedirectDirectoryService],
})
export class RedirectDirectoryModule { }
