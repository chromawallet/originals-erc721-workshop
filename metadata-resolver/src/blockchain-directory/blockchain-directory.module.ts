import { Module } from '@nestjs/common';
import { BlockchainDirectoryService } from './blockchain-directory.service';

@Module({
  providers: [BlockchainDirectoryService],
  exports: [BlockchainDirectoryService],
})
export class BlockchainDirectoryModule {}
