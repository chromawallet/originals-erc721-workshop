import { Injectable, NotFoundException } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { Blockchain, Postchain } from 'ft3-lib';
import { ContractBlockchainMapping } from 'src/models/contract-mapping.model';

@Injectable()
export class BlockchainDirectoryService {
  private blockchains = new Map<string, Blockchain>();

  constructor(private readonly config: ConfigService) {
    this.initialize().catch((error) => {
      console.error('Error initializing blockchain directory:', error);
      process.exit(1);
    });
  }

  getBlockchain(contract: string) {
    console.log(`Getting blockchain for contract ${contract}`);
    console.log(`Available blockchains: ${Array.from(this.blockchains.keys()).join(', ')}`);
    const bc = this.blockchains.get(contract.toLowerCase());

    if (bc == null) {
      throw new NotFoundException(
        `Blockchain not found for contract ${contract}`,
      );
    }

    return bc;
  }

  async initialize() {
    const mappings = this.config.get<ContractBlockchainMapping[]>('contractBlockchainMappings');

    for (const mapping of mappings) {
      this.blockchains.set(
        mapping.contract.toLowerCase(),
        await new Postchain(mapping.blockchain.url).blockchain(
          mapping.blockchain.rid,
        ),
      );
    }
  }

  customRetryStrategy(err: Error, response, body, options) {
    if (response && response.attempts >= options.maxAttempts) {
      console.log(`Failed with status code: ${response.statusCode}`);
      if (body) console.log(body);
      if (err) console.error(err);
    }

    return !!err || response.statusCode !== 200 || body.includes('error');
  }
}
