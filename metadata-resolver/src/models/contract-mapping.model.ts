export interface ContractBlockchainMapping {
  contract: string;
  blockchain: Blockchain;
}

export interface ContractRedirectMapping {
  contract: string;
  url: string;
}

interface Blockchain {
  url: string;
  rid: string;
}
