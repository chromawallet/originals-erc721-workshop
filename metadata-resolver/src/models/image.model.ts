export default interface Image {
  data: Buffer;
  contentType: string;
}
