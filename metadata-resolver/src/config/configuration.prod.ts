export default () => ({
  port: parseInt(process.env.PORT, 10) || 3000,
  contractBlockchainMappings: [
    {
      contract: '0xe218144c228863b03ccf85d120fd5b71bf97f3f4',
      blockchain: {
        url: 'http://metachain:7740',
        rid: 'f229955977d5df3fe2d3da59074124040b781c2709c79014c76701f7c1ac23e7',
      },
    },
  ],
  contractRedirectMappings: [
    {
      contract: '0x820aa19b7c82eb4294691d82475ee4175e8c2485',
      url: 'https://metadata.chainofalliance.com/',
    },
  ],
});
