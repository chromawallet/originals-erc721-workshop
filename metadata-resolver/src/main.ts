import { Logger } from '@nestjs/common';
import { ConfigService } from '@nestjs/config';
import { NestFactory } from '@nestjs/core';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';
import { json, urlencoded } from 'express';
import { AppModule } from './app.module';
import * as compression from 'compression';

async function bootstrap() {
  const app = await NestFactory.create(AppModule);

  app.use(json({ limit: '50mb' }));
  app.use(urlencoded({ extended: true, limit: '50mb' }));
  app.use(compression());

  const config = new DocumentBuilder()
    .setTitle('Originals Metadata Resolver')
    .setDescription('Resolver for Chromia Originals Metadata')
    .setVersion('1.0')
    .addTag('resolver')
    .build();
  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('api', app, document);

  const appConfig = app.get<ConfigService>(ConfigService);
  const logger = new Logger('Bootstrap');

  const port = appConfig.get('port');

  app.enableCors();

  logger.log(`Listening on port ${port}`);
  await app.listen(port);
}
bootstrap();
