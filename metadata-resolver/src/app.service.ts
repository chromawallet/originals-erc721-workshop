import {
  ConflictException,
  Injectable,
  NotFoundException,
} from '@nestjs/common';
import { BlockchainDirectoryService } from './blockchain-directory/blockchain-directory.service';
import { RedirectDirectoryService } from './redirect-directory/redirect-directory.service';
import { Filehub } from '@snieking/fs-client';
import Image from './models/image.model';

@Injectable()
export class AppService {
  constructor(
    private readonly blockchainDirectory: BlockchainDirectoryService,
    private readonly redirectDirectory: RedirectDirectoryService,
  ) { }

  getRedirectUrl(chain: string, contract: string, id: string): string {
    const url = this.redirectDirectory.getUrl(contract);
    return url == null ? null : url + chain + "/" + contract + "/" + id;
  }

  getAttributes(chain: string, contract: string, id: string): unknown {
    const blockchain = this.blockchainDirectory.getBlockchain(contract);
    return blockchain.query('metadata.get_attributes_by_meta', {
      chain: chain,
      contract_address: contract,
      id: parseInt(id, 10),
      attribute_annotation: "metadata_attribute",
      remove_interfaces: true,
    });
  }

  async getImage(chain: string, contract: string, id: string): Promise<Image> {
    const blockchain = this.blockchainDirectory.getBlockchain(contract);
    const imageDetails = await blockchain.query(
      'metadata.get_image_storage_details',
      {
        chain: chain,
        contract_address: contract,
        id: parseInt(id),
      },
    );

    if (imageDetails == null) {
      throw new NotFoundException(`Metadata not found for id ${id}`);
    }

    if (
      imageDetails.hub_location == null ||
      imageDetails.hub_brid == null ||
      imageDetails.hash == null ||
      imageDetails.content_type == null
    ) {
      throw new ConflictException(
        `Invalid image [${imageDetails}] found for ${id}`,
      );
    }

    const filehub = new Filehub(
      imageDetails.hub_location,
      imageDetails.hub_brid,
    );
    const file = await filehub.getFile(imageDetails.hash);

    if (file == null || !file.data) {
      throw new ConflictException(
        `Image file not found in Filehub for ${imageDetails.hash}`,
      );
    }

    return {
      data: file.data,
      contentType: imageDetails.content_type,
    };
  }
}
